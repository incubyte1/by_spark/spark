#!/usr/bin/python3
from pyspark.sql import SparkSession


# create SparkSession object
spark = SparkSession.builder\
            .appName("customer")\
            .master("local[2]")\
            .enableHiveSupport()\
            .getOrCreate()

patient = spark.read\
            .option("header", "true")\
            .option("inferSchema", "true")\
            .option("nullValue", "NULL")\
            .option("mode", "DROPMALFORMED")\
            .option("delimiter", '|')\
            .csv("/home/arvind/Desktop/customer.csv")

patient = patient.drop('H','_c0')


patient.write\
    .partitionBy("Country")\
    .saveAsTable("cust_country_part", format="orc", mode="OVERWRITE")\


print("cust_country_part table written.")



patient.show(truncate=False)
query = input("Enter query to get information of customers of perticular country ")
result = spark.sql(query)
# "Select * from cust_country_part where Country='IND'"
result.show(truncate=False)
spark.stop()


# output
# cust_country_part table written.
# +-------------+-----------+---------+-------------------+--------------+-------+-----+-------+-------+---------+
# |Customer_Name|Customer_Id|Open_Date|Last_Consulted_Date|Vaccination_Id|Dr_Name|State|Country|DOB    |Is_Active|
# +-------------+-----------+---------+-------------------+--------------+-------+-----+-------+-------+---------+
# |Alex         |123457     |20101012 |20121013           |MVD           |Paul   |SA   |USA    |6031987|null     |
# |John         |123458     |20101012 |20121013           |MVD           |Paul   |TN   |IND    |6031987|A        |
# |Mathew       |123459     |20101012 |20121013           |MVD           |Paul   |WAS  |PHIL   |6031987|A        |
# |Matt         |12345      |20101012 |20121013           |MVD           |Paul   |BOS  |NYC    |6031987|A        |
# |Jacob        |1256       |20101012 |20121013           |MVD           |Paul   |VIC  |AU     |6031987|A        |
# +-------------+-----------+---------+-------------------+--------------+-------+-----+-------+-------+---------+
#
# Enter query to get information of customers of perticular country Select * from cust_country_part where Country='IND'
# +-------------+-----------+---------+-------------------+--------------+-------+-----+-------+---------+-------+
# |Customer_Name|Customer_Id|Open_Date|Last_Consulted_Date|Vaccination_Id|Dr_Name|State|DOB    |Is_Active|Country|
# +-------------+-----------+---------+-------------------+--------------+-------+-----+-------+---------+-------+
# |John         |123458     |20101012 |20121013           |MVD           |Paul   |TN   |6031987|A        |IND    |
# +-------------+-----------+---------+-------------------+--------------+-------+-----+-------+---------+-------+